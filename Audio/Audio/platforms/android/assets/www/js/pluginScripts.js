function vocal(){
    // start audio capture
    navigator.device.capture.captureAudio(captureSuccess, captureError, {limit:1});

    // capture callback
    function captureSuccess(mediaFiles) {
        var i, path, len;
        for (i = 0, len = mediaFiles.length; i < len; i += 1) {
            path = mediaFiles[i].fullPath;
            // do something interesting with the file
            alert(path);
            StuVocal = path;
        }
    };

// capture error callback
    function captureError(error) {
        alert('Error code: ' + error.code);
    };
}

function snap(){
    navigator.camera.getPicture(onSuccess, onFail, {
        quality:100,
        targetHeight:200,
        targetWidth:200,
        destinationType: Camera.DestinationType.DATA_URL,
        encodingType: Camera.EncodingType.JPEG,
        sourceType : Camera.PictureSourceType.CAMERA,
    });

    function onSuccess(imageData) {
        StuImage = "data:image/jpeg;base64," + imageData;
    }

    function onFail(message) {
        alert('Failed because: ' + message);
    }

}

function video(){
    // start video capture
    navigator.device.capture.captureVideo(captureSuccess, captureError, {limit:1});
        // capture callback
    function captureSuccess(mediaFiles) {
        var i, path, len;
        for (i = 0, len = mediaFiles.length; i < len; i += 1) {
            path = mediaFiles[i].fullPath;
            // do something interesting with the file
            alert(path);
            StuVideo = path;
        }
    };

    // capture error callback
    function captureError(error) {
        navigator.notification.alert('Error code: ' + error.code, null, 'Capture Error');
    };
}